// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API: 'https://test-api.live.gbksoft.net/rest/v1/',
  // tslint:disable-next-line:max-line-length
  FACEBOOK_URL: 'https://www.facebook.com/dialog/oauth?client_id=1637129439866047&response_type=token&redirect_uri=https://test-api.live.gbksoft.net&scope=public_profile,email',
  GOOGLE_API_KEY: 'AIzaSyAVpzeQ5eaI5OI1bUdT1mmAL0XxO3nGBLo'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
