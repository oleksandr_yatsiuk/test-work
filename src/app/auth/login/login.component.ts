import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RootService } from 'src/app/shared/root.module';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalComponent } from 'src/app/auth/modal/modal.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  animal: string;
  name: string;
  loading = false;
  constructor(public dialog: MatDialog, private router: Router, private http: RootService, private formBuilder: FormBuilder) { }

  loginForm: FormGroup;

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  openDialog(): void {
    this.dialog.open(ModalComponent);
  }


  onSubmit(): void {
    if (this.loginForm.valid) {
      this.loading = true;
      this.http.simpleLogin(this.loginForm.value).subscribe(({code, result}) => {
        if (code === 201) {
          this.loading = false;
          localStorage.setItem('auth', JSON.stringify(result));
          this.router.navigate(['board/profile']);
        }
      });

    }

  }
  hasError(control: string, error: string): boolean {
    return this.loginForm.controls[control].hasError(error);
  }

}
