import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RootService } from '../../shared/root.module';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  myForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private http: RootService, private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      url: ['', [Validators.required]]
    });
  }

  openAuthUrl(): void {
    window.open(environment.FACEBOOK_URL, '_blank');
  }

  onSubmit(): void {
    if (this.myForm.valid) {
      const url = this.myForm.value.url.split('&');
      const fbToken = url[0].split('=')[1];
      this.http.login(fbToken).subscribe(({ code, result }) => {
        if (code === 201) {
          localStorage.setItem('auth', JSON.stringify(result));
          this.dialog.closeAll();
          this.router.navigate(['board/profile']);
        }
      });
    }

  }
  hasError(control: string, error: string): boolean {
    return this.myForm.controls[control].hasError(error);
  }
  onNoClick(): void {
    this.dialog.closeAll();
  }
}
