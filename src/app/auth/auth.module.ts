import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/material.module';
import { ModalComponent } from './modal/modal.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    LoginComponent,
    ModalComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    MaterialModule
  ],
  entryComponents: [
    ModalComponent
  ]
})
export class AuthModule { }
