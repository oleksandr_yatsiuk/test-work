import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login, User } from '../auth/user.model';


@Injectable({ providedIn: 'root' })

export class RootService {
  private serverUrl = environment.API;

  constructor(private http: HttpClient) {
  }
  login(token: string): Observable<any> {
    return this.http.post<any>(`${this.serverUrl}user/login/facebook`, { token });
  }
  simpleLogin(data: Login): Observable<any> {
    return this.http.post<Login>(`${this.serverUrl}user/login`, data);
  }

  logout(): Observable<any> {
    return this.http.post<null>(`${this.serverUrl}user/logout`, null);
  }
  getCurrentUser(): Observable<any> {
    return this.http.get<User>(`${this.serverUrl}user/current`);
  }
  editUser(data): Observable<any> {
    return this.http.put<User>(`${this.serverUrl}user/profile`, data);
  }
  uploadImage(file: FormData): Observable<any> {
    return this.http.post<any>(`${this.serverUrl}user/profile/image`, file);
  }
  userList(perPage: number, page: number): Observable<any> {
    return this.http.get<any>(`${this.serverUrl}user?perPage=${perPage}&page=${page}`);
  }
  userOverview(id: number): Observable<any> {
    return this.http.get<any>(`${this.serverUrl}user/${id}`);
  }
  searchUsersByString(value: string): Observable<[any]> {
    return this.http.get<any>(`${this.serverUrl}user/search?searchString=${value}`);
  }
  searchUsersByLocation(radius: number, lat: number, lon: number): Observable<any> {
    return this.http.get<any>(`${this.serverUrl}user/search?radius=${radius}&lat=${lat}&lon=${lon}`);
  }
  updateUserLocation(data): Observable<any> {
    return this.http.put<any>(`${this.serverUrl}user/location`, data);
  }

}
