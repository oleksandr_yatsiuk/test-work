import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadderComponent } from './layout/headder/headder.component';

import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [HeadderComponent],
  imports: [
    RouterModule,
    CommonModule,
    MaterialModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    HeadderComponent,
    NgbModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule],
})
export class SharedModule { }
