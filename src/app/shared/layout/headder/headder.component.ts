import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RootService } from '../../root.module';
import { Router } from '@angular/router';
import { SearchService } from '../../../core/services/search.service';

@Component({
  selector: 'app-headder',
  templateUrl: './headder.component.html',
  styleUrls: ['./headder.component.scss']
})
export class HeadderComponent implements OnInit {
  public user: string;

  constructor(private http: RootService, private router: Router, private searchService: SearchService) { }

  ngOnInit(): void {
    this.http.getCurrentUser().subscribe(res => {
      this.user = res.result;
    });
  }

  logout(): void {
    this.http.logout().subscribe(req => {
      if (!req) {
        localStorage.removeItem('auth');
        this.router.navigate(['/login']);
      }
    });
  }
  updateService($event): void {
    this.searchService.doSearch($event);
  }
}
