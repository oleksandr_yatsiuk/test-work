import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list';
import { ProfileComponent, ProfileOverviewComponent, ProfileOverviewResolver } from './profile';
import { DashboardComponent } from './dashboard.component';
import { MapComponent } from './map/map.component';
import { AuthGuardService as AuthGuard } from '../core/guards/auth-guard.service';


const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'list', component: UsersListComponent, canActivate: [AuthGuard] },
      { path: 'map', component: MapComponent, canActivate: [AuthGuard], },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], },
      {
        path: 'profile/:id', component: ProfileOverviewComponent,
        resolve: { user: ProfileOverviewResolver }, canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
