import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { MaterialModule } from '../shared/material.module';
import { environment } from 'src/environments/environment';
import { UsersListComponent, UserCardComponent } from './users-list';
import { ProfileOverviewComponent, ProfileComponent } from './profile';
import { MapComponent } from './map/map.component';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  declarations: [
    MapComponent,
    UsersListComponent,
    DashboardComponent,
    ProfileOverviewComponent,
    ProfileComponent,
    UserCardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: environment.GOOGLE_API_KEY,
    }),
    AgmJsMarkerClustererModule,
    MaterialModule
  ]
})
export class DashboardModule { }
