import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { RootService } from 'src/app/shared/root.module';

@Injectable()
export class ProfileOverviewResolver implements Resolve<any> {
  constructor(private http: RootService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.http.userOverview(route.params.id);
  }
}
