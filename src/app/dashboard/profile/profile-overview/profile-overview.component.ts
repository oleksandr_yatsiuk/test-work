import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile-overview',
  templateUrl: './profile-overview.component.html',
  styleUrls: ['./profile-overview.component.scss']
})
export class ProfileOverviewComponent implements OnInit {
  user: any;
  constructor(private route: ActivatedRoute, private router: Router, private location: Location) {
  }

  ngOnInit(): void {
    this.user = this.route.snapshot.data.user.result;
  }
  returnToList(): void {
    this.location.back();
  }
}
