import { Component, OnInit } from '@angular/core';
import { RootService } from 'src/app/shared/root.module';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { pluck } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public user: any;
  myProfile: FormGroup;
  url: any;
  selectedFile: File = null;
  imagePath: File = null;
  uploadIsActive = false;
  loading = false;
  uploading = false;
  constructor(private formBuilder: FormBuilder, private http: RootService, private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.getForm();

    this.http.getCurrentUser().pipe(pluck('result')).subscribe(user => {
      this.user = user;
      this.url = user.image;
      this.myProfile.setValue({
        firstName: user.firstName,
        lastName: user.lastName,
        country: user.country,
        city: user.city,
        gender: user.gender,
      });
    });

  }

  hasError(control: string, error: string): boolean {
    return this.myProfile.controls[control].hasError(error);
  }

  onSubmit(): void {
    if (this.myProfile.valid) {
      this.loading = true;
      this.http.editUser(this.myProfile.value).pipe(pluck('result')).subscribe(({ id }) => {
        if (id) { location.reload(); }
      });
    }
  }

  getForm(): void {
    this.myProfile = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      country: ['', [Validators.required]],
      city: ['', [Validators.required]],
      gender: ['', [Validators.required]],
    });
  }

  onFileSelected(event): void {
    this.selectedFile = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      this.uploadIsActive = true;
      const reader = new FileReader();
      this.imagePath = event.target.files;
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.url = reader.result;
      };
    }
  }
  upload(): void {
    if (this.selectedFile !== null) {
      this.uploading = true;
      this.uploadIsActive = true;
      const fd = new FormData();
      fd.append('image', this.selectedFile, this.selectedFile.name);
      this.http.uploadImage(fd).subscribe(({ code }) => {
        if (code === 200) {
          this.uploading = false;
          this.uploadIsActive = false;
        }
      });
    }
  }
}
