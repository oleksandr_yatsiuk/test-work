import { Component, OnInit } from '@angular/core';
import { RootService } from 'src/app/shared/root.module';
import { pluck } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/core/services/search.service';
import { calculateDistance } from 'src/app/core/utils/maps-support';
import { Marker, Coords } from './map.models';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  markers: Marker[];
  defaultRadius = 10;
  zoom = 11;
  map: any;
  defaultImage = '../../../assets/data/default_profile_image.png';
  userCoords: Coords = {
    lat: 50.49513571381661,
    lng: 30.47153200235219
  };
  constructor(private http: RootService, private router: Router, private searchService: SearchService) { }

  ngOnInit(): void {
    this.getCurrentUserPosition();
    this.trackingPosition();
    this.search();
  }

  search(): void {
    this.searchService.searchString.subscribe(searchedList => {
      this.setMarkets(searchedList);
    });
  }
  
  mapReady(data: any): void {
    this.map = data;
  }

  setUserCoords(lat: number, lng: number): void {
    if (this.map) {
      this.map.setCenter({ lat, lng });
    }
    this.userCoords = { lat, lng };
  }

  trackingPosition(): void {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((response) => {
        const { latitude, longitude } = response.coords;
        const distance = calculateDistance(latitude, longitude, this.userCoords.lat, this.userCoords.lng);
        if (response && distance > 1) {
          this.http.updateUserLocation({ lat: latitude, lon: longitude }).subscribe(res => {
            this.getCurrentUserPosition();
          });
        }
      }, (err => { console.log(err); }), {
        maximumAge: 60 * 1000
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }

  // check position from backend or update location by navigator geolocation
  getCurrentUserPosition(): void {
    this.http.getCurrentUser()
      .pipe(pluck('result'))
      .subscribe(({ lat, lon }) => {
        if (lat && lon) {
          this.setUserCoords(+lat, +lon);
          this.addMarkers(this.defaultRadius, lat, lon);
        } else {
          navigator.geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords;
            this.setUserCoords(latitude, longitude);
            this.addMarkers(this.defaultRadius, +latitude, +longitude);
          });
        }
      });
  }

  addMarkers(radius, lat, lng): void {
    this.http.searchUsersByLocation(radius, lat, lng)
      .pipe(pluck('result'))
      .subscribe(result => {
        this.setMarkets(result);
      });
  }

  setMarkets(markers): void {
    this.markers = markers.map(user => {
      user.image === null ? user.image = this.defaultImage : user.image = user.image;
      return {
        lat: +user.lat,
        lng: +user.lon,
        label: `${user.firstName} ${user.lastName}`,
        draggable: false,
        icon: this.defaultImage,
        location: `${user.country},${user.city}`,
        id: user.id,
      };
    });
  }

  clickedMarker(userId: number): void {
    this.router.navigate([`/board/profile/${userId}`]);
  }

  // update markers after changing circle radius
  change($event: any): void {
    this.defaultRadius = $event / 1000;
    this.addMarkers(this.defaultRadius, this.userCoords.lat, this.userCoords.lng);
  }

  // update markers after changing circle position
  circleStop($event: any): void {
    this.addMarkers(this.defaultRadius, $event.coords.lat, $event.coords.lng);
  }
}

