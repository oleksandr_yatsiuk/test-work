import { Component, OnInit, Output, EventEmitter, Input, AfterContentChecked, AfterViewInit, OnChanges } from '@angular/core';
import { RootService } from 'src/app/shared/root.module';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/core/services/search.service';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  constructor(private http: RootService, private router: Router, private searchService: SearchService) { }

  public users;
  public searchString;
  public loading = false;

  length: number;
  pageSize = 20;
  pageCount: number;
  currentPage: 1;
  paginationIsVisiable = true;
  lastPages = false;

  ngOnInit(): void {
    this.getUserList(this.currentPage);
    this.findUsers();
    this.paginationIsVisiable = true;
  }

  findUsers(): void {
    this.searchService.searchString.subscribe(usersList => {
      usersList ? this.users = usersList : this.ngOnInit();
      this.paginationIsVisiable = false;
    });
  }

  getUserList(page): void {
    this.loading = true;
    this.http.userList(this.pageSize, page)
      .subscribe(({ result, _meta }) => {
        this.pageCount = _meta.pagination.pageCount;
        this.currentPage = _meta.pagination.currentPage;
        this.length = result.length;
        this.users = result.filter(profile => profile.email);
        this.loading = false;
      });
    this.searchService.doSearch(this.searchString);
  }

  page(page): void {
    this.getUserList(page);
  }
}
