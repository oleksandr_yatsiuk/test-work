export interface Coords {
    lat: number;
    lng: number;
}

export interface Marker extends Coords {
    id: number;
    label: string;
    icon: string;
    draggable: boolean;
    location: string;
}
