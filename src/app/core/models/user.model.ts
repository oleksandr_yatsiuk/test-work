export interface Login {
  email: string;
  password: string;
}
export interface User {
  country: string | null;
  city: string | null;
  firstName: string | null;
  lastName: string | null;
  email: string;
  image?: string | null;
  lat?: number | 0;
  lon?: number | 0;
  gender: string | null;
  createdAt?: number;
  updatedAt?: number;
}
