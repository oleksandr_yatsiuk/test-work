import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return req.url.includes(environment.API) ? next.handle(this.setToken(req)) : next.handle(req);
  }

  get token() {
    const storage = JSON.parse(localStorage.getItem('auth'));
    return storage ? storage.token : false;
  }
  
// add bearer token before each request if auth token present in localStorage
  private setToken = (req) => req.clone({
    headers: req.headers.set('Authorization', `Bearer ${this.token}`)
  })
}
