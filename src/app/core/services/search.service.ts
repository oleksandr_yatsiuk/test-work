import { Injectable, EventEmitter } from '@angular/core';
import { RootService } from '../../shared/root.module';
import { pluck } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  public searchString: EventEmitter<any> = new EventEmitter();
  constructor(private http: RootService) { }

  // set word in search input and return list of users by value
  doSearch(word: string): void {
    if (word && word.length > 0) {
      setTimeout(() => {
        this.http.searchUsersByString(word).pipe(pluck('result')).subscribe(userList => this.searchString.emit(userList));
      }, 500);
    }
  }
}
