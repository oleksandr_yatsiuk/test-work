import { Injectable } from '@angular/core';
import { RootService } from 'src/app/shared/root.module';
@Injectable()
export class AuthService {
    constructor(public http: RootService) { }

    public isAuthenticated(): boolean {
        return this.isTokenNotExpired();
    }

    // Check whether the token is expired and return true or false
    public isTokenNotExpired(): boolean {
        const { expiredAt } = JSON.parse(localStorage.getItem('auth'));
        return Date.now() / 1000 < expiredAt ? true : false;
    }
}
