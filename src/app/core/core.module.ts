import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RootService } from '../shared/root.module';
import { ProfileOverviewResolver } from '../dashboard/profile/profile-overview/profile-overview.resolver';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ErrorInterseptor } from './interceptors/error.interceptor';
import { SearchService } from './services/search.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [RootService,
    ProfileOverviewResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterseptor,
      multi: true
    },
    SearchService,
    AuthGuardService,
    AuthService
  ],
})
export class CoreModule { }
